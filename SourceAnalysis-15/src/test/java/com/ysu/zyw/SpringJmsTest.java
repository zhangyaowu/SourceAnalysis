package com.ysu.zyw;

import org.junit.Test;

/**
 * Created by ZeRur on 2016/1/15.
 *
 * @author yaowu.zhang
 */
public class SpringJmsTest {

    @Test
    public void testP2P() throws InterruptedException {
        new Thread(new Consumer(0)).start();
        new Thread(new Producer(0)).start();
        Thread.sleep(99999);
    }

    @Test
    public void testTopic() throws InterruptedException {
        new Thread(new Consumer(1)).start();
        new Thread(new Consumer(3)).start();
        new Thread(new Consumer(5)).start();
        new Thread(new Producer(1)).start();
        Thread.sleep(99999);
    }

}
