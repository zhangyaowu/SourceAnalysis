package com.ysu.zyw;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;

/**
 * Created by ZeRur on 2016/1/15.
 *
 * @author yaowu.zhang
 */
public class Consumer implements Runnable {

    private int id;

    public Consumer(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("consumer start ...");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context-jms-consumer.xml");
        JmsTemplate jmsTemplate = applicationContext.getBean("jmsTemplate", JmsTemplate.class);
        try {
            Thread.sleep(99999);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
