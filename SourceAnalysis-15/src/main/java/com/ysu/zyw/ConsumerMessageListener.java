package com.ysu.zyw;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * Created by ZeRur on 2016/1/16.
 *
 * @author yaowu.zhang
 */
public class ConsumerMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        ObjectMessage objectMessage = (ObjectMessage) message;
        try {
            System.out.println(objectMessage.getObject());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

}
