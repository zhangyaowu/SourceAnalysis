package com.ysu.zyw;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ZeRur on 2016/1/16.
 *
 * @author yaowu.zhang
 */
public class Model implements Serializable {

    private int id;

    private String name;

    private Date date;

    public Model() {
    }

    public Model(int id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
