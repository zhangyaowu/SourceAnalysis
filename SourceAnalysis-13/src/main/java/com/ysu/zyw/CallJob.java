package com.ysu.zyw;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Created by ZeRur on 2016/1/14.
 *
 * @author yaowu.zhang
 */
public class CallJob extends QuartzJobBean {

    private int id;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        System.out.println("call ... " + id + " - " + context.getJobDetail().getJobDataMap().get("who"));

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
