package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by ZeRur on 2016/1/14.
 *
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-schedule.xml"})
public class ScheduleTest {

    @Test
    public void test1() {
        // taskExecutor 是 Spring 实现的线程池 executorService 是 jdk 自带的线程池实现
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.initialize();
        for (int i = 0; i < 3; i++)
            taskExecutor.execute(() -> {
                for (int j = 0; j < 3; j++)
                    System.out.println("helo - " + j);
            });
    }

    @Test
    public void test2() throws InterruptedException {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.initialize();
        taskScheduler.schedule(() -> System.out.println(new Date()), new PeriodicTrigger(10, TimeUnit.SECONDS));
        Thread.sleep(999999);
    }

    @Test
    public void test3() throws InterruptedException {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.initialize();
        taskScheduler.schedule(() -> System.out.println(new Date()), new CronTrigger("0,10,20,30,40,50 * * * * *"));
        Thread.sleep(999999);
    }

    @Test
    public void test4() throws ExecutionException, InterruptedException {
        ThreadPoolTaskExecutor taskScheduler = new ThreadPoolTaskExecutor();
        taskScheduler.initialize();
        Future<Integer> future = taskScheduler.submit(() -> {
            Thread.sleep(3000);
            return 100;
        });
        System.out.println(future.get());
    }

    @Test
    public void test5() throws InterruptedException {
        Thread.sleep(999999);
    }

}
