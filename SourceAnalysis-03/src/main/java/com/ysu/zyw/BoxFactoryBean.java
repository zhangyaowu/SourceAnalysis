package com.ysu.zyw;

import org.springframework.beans.factory.FactoryBean;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class BoxFactoryBean implements FactoryBean<Box> {

    public Box getObject() throws Exception {
        return new Box();
    }

    public Class<?> getObjectType() {
        return Box.class;
    }

    public boolean isSingleton() {
        return false;
    }

}
