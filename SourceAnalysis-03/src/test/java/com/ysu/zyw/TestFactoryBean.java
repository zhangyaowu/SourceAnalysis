package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-factory-bean.xml" })
public class TestFactoryBean {

    @Resource
    private ApplicationContext applicationContext;

    @Test
    public void testFactoryBean() {
        System.out.println(applicationContext.getBean("box"));
        System.out.println(applicationContext.getBean("&box"));
    }

}
