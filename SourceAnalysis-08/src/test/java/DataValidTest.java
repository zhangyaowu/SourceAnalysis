import com.ysu.zyw.Bag;
import com.ysu.zyw.demo1.Person1;
import com.ysu.zyw.demo2.Person2;
import com.ysu.zyw.demo3.Person3;
import com.ysu.zyw.demo4.CREATE;
import com.ysu.zyw.demo4.Person4;
import com.ysu.zyw.demo4.UPDATE;
import com.ysu.zyw.demo5.Person5;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.internal.util.scriptengine.ScriptEvaluatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.scripting.ScriptEvaluator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.script.ScriptException;
import javax.validation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by ZeRur on 2016/1/8.
 *
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-dv.xml"})
public class DataValidTest {

    private Validator validator;

    @Before
    public void prepare() {
        HibernateValidatorConfiguration config = Validation.byProvider(HibernateValidator.class).configure();
        ValidatorFactory factory = config.buildValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void test1() {
        Person1 person1 = new Person1();
        person1.setId(100);
        person1.setName("yaowu");
        person1.setPassword("123456");
        person1.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person1.setBag(bag);

        Set<ConstraintViolation<Person1>> constraintViolationSet = validator.validate(person1);
        Assert.isTrue(constraintViolationSet.size() == 1);

        constraintViolationSet.stream().forEach(i -> System.out.println(i));
    }

    @Test
    public void test2() {
        Person2 person2 = new Person2();
        person2.setId(100);
        person2.setName("yaowu");
        person2.setPassword("123456");
        person2.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person2.setBag(bag);

        Set<ConstraintViolation<Person2>> constraintViolationSet = validator.validate(person2);
        Assert.isTrue(constraintViolationSet.size() == 1);

        constraintViolationSet.stream().forEach(i -> System.out.println(i.getMessage()));
    }

    @Test
    public void test3() {
        Person3 person3 = new Person3();
        person3.setId(100);
        // person3.setName("yaowu");
        person3.setPassword("123456");
        person3.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person3.setBag(bag);

        // name 为 null 是不会再做其他校验的 需要添加 @NotNull 规则
        Set<ConstraintViolation<Person3>> constraintViolationSet = validator.validate(person3);
        Assert.isTrue(constraintViolationSet.size() == 1);

        constraintViolationSet.stream().forEach(i -> System.out.println(i.getMessage()));
    }

    @Test
    public void test4() {
        Person4 person4 = new Person4();
        person4.setId(100);
        person4.setName("yaowu");
        person4.setPassword("123456");
        person4.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person4.setBag(bag);

        Set<ConstraintViolation<Person4>> createConstraintViolationSet = validator.validate(person4, CREATE.class);
        Assert.isTrue(createConstraintViolationSet.size() == 2);

        createConstraintViolationSet.stream().forEach(i -> System.out.println(i.getMessage()));


        Set<ConstraintViolation<Person4>> updateConstraintViolationSet = validator.validate(person4, UPDATE.class);
        Assert.isTrue(updateConstraintViolationSet.size() == 1);

        updateConstraintViolationSet.stream().forEach(i -> System.out.println(i.getMessage()));
    }

    @Test
    public void test5() {
        Person5 person5 = new Person5();
        person5.setId(100);
        person5.setName("yaowu");
        person5.setPassword("123456");
        person5.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person5.setBag(bag);

        Set<ConstraintViolation<Person5>> constraintViolationSet = validator.validate(person5);
        System.out.println(constraintViolationSet);
        Assert.isTrue(constraintViolationSet.size() == 2);

        constraintViolationSet.stream().forEach(i -> System.out.println(i.getMessage()));
    }

    @Resource
    private Validator coreContainerValidator;

    @Test
    public void test6() {
        Person1 person1 = new Person1();
        person1.setId(101);
        person1.setName("yaowu");
        person1.setPassword("123456");
        person1.setPasswordConfirm("1234567");
        List<String> foods = new ArrayList<>();
        foods.add("apple");
        Bag bag = new Bag();
        bag.setFoods(foods);
        person1.setBag(bag);

        Set<ConstraintViolation<Person1>> constraintViolationSet = coreContainerValidator.validate(person1);
        Assert.isTrue(constraintViolationSet.size() == 1);

        constraintViolationSet.stream().forEach(i -> System.out.println(i));
    }

}
