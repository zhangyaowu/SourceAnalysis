package com.ysu.zyw.demo5;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by ZeRur on 2016/1/9.
 * @author yaowu.zhang
 */
@Target(value = {ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
//指定验证器
@Constraint(validatedBy = PersonValidator.class)
@Documented
public @interface ValidPerson {

    String message() default "";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
