package com.ysu.zyw.demo5;

import com.ysu.zyw.Bag;
import com.ysu.zyw.demo4.CREATE;
import com.ysu.zyw.demo4.UPDATE;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by ZeRur on 2016/1/9.
 *
 * @author yaowu.zhang
 */
@ValidPerson
public class Person5 {

    private int id;

    @Size(min = 3, max = 4)
    private String name;

    private String password;

    private String passwordConfirm;

    private Bag bag;

    @Override
    public String toString() {
        return "Person1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirm='" + passwordConfirm + '\'' +
                ", bag=" + bag +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

}
