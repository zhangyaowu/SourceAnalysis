package com.ysu.zyw.demo3;

import com.sun.istack.internal.NotNull;
import com.ysu.zyw.Bag;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * Created by ZeRur on 2016/1/9.
 * @author yaowu.zhang
 */
public class Person3 {

    private int id;

    @Length(min = 1, max = 3, message = "名字[${validatedValue}]必须在{min}, {max}之间")
    private String name;

    @NotEmpty
    @Pattern(regexp = "[a-z]{1,3}")
    private String password;

    private String passwordConfirm;

    private Bag bag;

    @Override
    public String toString() {
        return "Person1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirm='" + passwordConfirm + '\'' +
                ", bag=" + bag +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

}
