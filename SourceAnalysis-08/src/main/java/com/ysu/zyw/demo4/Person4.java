package com.ysu.zyw.demo4;

import com.ysu.zyw.Bag;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * Created by ZeRur on 2016/1/9.
 *
 * @author yaowu.zhang
 */
public class Person4 {

    @Min(value = 1, groups = {CREATE.class})
    @Max(value = 3, groups = {CREATE.class})
    private int id;

    @Pattern(regexp = "[a-z]{1,3}", groups = {CREATE.class})
    private String name;

    @Pattern(regexp = "[a-z]{1,3}", groups = {UPDATE.class})
    private String password;

    private String passwordConfirm;

    private Bag bag;

    @Override
    public String toString() {
        return "Person1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirm='" + passwordConfirm + '\'' +
                ", bag=" + bag +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

}
