package com.ysu.zyw;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by ZeRur on 2016/1/9.
 * @author yaowu.zhang
 */
public class Bag {

    @Size(min = 1, max = 3)
    private List<String> foods;

    @Override
    public String toString() {
        return "Bag{" +
                "foods=" + foods +
                '}';
    }

    public List<String> getFoods() {
        return foods;
    }

    public void setFoods(List<String> foods) {
        this.foods = foods;
    }

}
