package com.ysu.zyw;

import java.lang.annotation.*;

/**
 * Created by ZeRur on 2016/1/15.
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cuter {
}
