package com.ysu.zyw;

/**
 * Created by ZeRur on 2016/1/14.
 *
 * @author yaowu.zhang
 */
public class Aspect {

    public void executionWithParamMethod(String param) {
        System.out.println(param + "executionWithParamMethod ...");
    }

    public void executionMethod() {
        System.out.println("executionMethod ...");
    }

    public void withinMethod() {
        System.out.println("withinMethod ...");
    }

    public void targetMethod() {
        System.out.println("targetMethod ...");
    }

    public void thisMethod() {
        System.out.println( "thisMethod ...");
    }

    public void argsMethod() {
        System.out.println("argsMethod ...");
    }

    public void withinAnnotationMethod() {
        System.out.println("withinAnnotationMethod ...");
    }

    public void targetAnnotationMethod() {
        System.out.println("targetAnnotationMethod ...");
    }

    public void argsAnnotationMethod() {
        System.out.println("argsAnnotationMethod ...");
    }

    public void annotationMethod() {
        System.out.println("annotationMethod ...");
    }

    public void beanMethod() {
        System.out.println("beanMethod ...");
    }

    public void paramTestMethod() {
        System.out.println("param test method ...");
    }

}
