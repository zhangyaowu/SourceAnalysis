package com.ysu.zyw;

/**
 * Created by ZeRur on 2016/1/14.
 *
 * @author yaowu.zhang
 */
@Cuter
public class Model {

    public void executionWithParamRunner(String param) {
        System.out.println(param + "executionWithParamRunner ...");
    }

    public void executionRunner() {
        System.out.println("executionRunner ...");
    }

    public void withinRunner() {
        System.out.println("withinRunner ...");
    }

    public void targetRunner() {
        System.out.println("targetRunner ...");
    }

    public void thisRunner() {
        System.out.println( "thisRunner ...");
    }

    public void argsRunner(String str, Integer in) {
        System.out.println("argsRunner ...");
    }

    public void withinAnnotationRunner() {
        System.out.println("withinAnnotationRunner ...");
    }

    public void targetAnnotationRunner() {
        System.out.println("targetAnnotationRunner ...");
    }

    public void argsAnnotationRunner() {
        System.out.println("argsAnnotationRunner ...");
    }

    public void annotationRunner() {
        System.out.println("annotationRunner ...");
    }

    public void beanRunner() {
        System.out.println("beanRunner ...");
    }

    public void paramTest(String str, @Cuter Integer in, Boolean bol) {
        System.out.println("param test ...");
    }

}
