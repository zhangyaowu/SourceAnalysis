package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2016/1/14.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-aspectj.xml"})
public class AspectJTest {

    @Resource
    private Model model;

    @Test
    public void test1() {
        // 方法执行 带参数
        model.executionWithParamRunner("Helo - ");
    }

    @Test
    public void test2() {
        // 方法执行
        model.executionRunner();
    }

    @Test
    public void test3() {
        // 指定类型
        model.withinRunner();
    }

    @Test
    public void test4() {
        // 指定类型（receiver perspective）
        model.targetRunner();
    }

    @Test
    public void test5() {
        // 指定类型（caller perspective）
        model.thisRunner();
    }

    @Test
    public void test6() {
        // 指定参数
        model.argsRunner("", 1);
    }

    @Test
    public void test7() {
        // 指定注解
        model.annotationRunner();
    }

    @Test
    public void test8() {
        model.paramTest("1", 2, false);
    }

}
