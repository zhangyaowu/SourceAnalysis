package com.ysu.zyw;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

/**
 * Created by ZeRur on 2016/1/17.
 *
 * @author yaowu.zhang
 */
public class LuceneTest {

    // sources
    private Map<Integer, User> userMap = new HashMap<>();

    @Before
    public void initData() {
        userMap.put(1, new User(1, "old lee", 20, "old lee is a good man, he always help others"));
        userMap.put(2, new User(2, "old wang", 20, "old wang is good at learning"));
        userMap.put(3, new User(3, "old zhang", 20, "old zhang is a normal person"));
        userMap.put(4, new User(4, "old sun", 20, "old sun is not really good at cooking"));
        userMap.put(5, new User(5, "old zhao", 20, "old zhao is very good at cooking"));
    }

    @Test
    public void test() throws IOException, ParseException {
        // 创建 Directory (FSDirectory.open() 可创建实际索引 内存索引不会保留)
        Directory directory = new RAMDirectory();
        index(directory);
        search(directory);
    }

    /**
     * 创建索引
     */
    public void index(Directory directory) throws IOException {
        // 创建 IndexWriter
        IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(new StandardAnalyzer()));
        // 创建文档
        for (User user : userMap.values()) {
            Document document = new Document();
            document.add(new IntField("id", user.getId(), Field.Store.YES));
            document.add(new StringField("name", user.getName(), Field.Store.YES));
            document.add(new TextField("desc", user.getDesc(), Field.Store.YES));
            document.add(new StoredField("user", new BytesRef(SerializerUtil.serialize(user))));

            indexWriter.addDocument(document);
        }
        indexWriter.close();
    }

    /**
     * 搜索
     */
    public void search(Directory directory) throws IOException, ParseException {
        // 创建 IndexReader
        IndexReader indexReader = DirectoryReader.open(directory);
        // 创建 IndexSearch
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        // 创建 QueryParser
        QueryParser queryParser = new QueryParser("desc", new StandardAnalyzer());


        Query query = queryParser.parse("lee");

        TopDocs topDocs = indexSearcher.search(query, 10);

        ScoreDoc[] scoreDocs = topDocs.scoreDocs;

        for (ScoreDoc scoreDoc : scoreDocs) {
            Document document = indexReader.document(scoreDoc.doc);
            System.out.println(document.get("id"));
            System.out.println(userMap.get(Integer.parseInt(document.get("id"))));
            System.out.println(SerializerUtil.deserialize(document.getBinaryValue("user").bytes, User.class));
        }

        indexReader.close();
    }

    /**
     * 删除索引
     */
    public void delete(Directory directory) throws IOException {
        IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(new StandardAnalyzer()));
        indexWriter.deleteAll();
        // Term 精确查找 Query 匹配查找
        // indexWriter.deleteDocuments(null);
    }

    public void update(Directory directory) throws IOException {
        IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(new StandardAnalyzer()));
        // Term 精确匹配更新 Query 匹配更新
        // indexWriter.updateDocument(null, null);
    }

}
