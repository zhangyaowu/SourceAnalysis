package com.ysu.zyw.circledependency;

import com.ysu.zyw.setter.ContainerA;
import com.ysu.zyw.setter.ContainerB;
import com.ysu.zyw.setter.ContainerC;
import org.junit.Test;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class CircleDependencyTest {

    @Test
    public void testSetterDependency() {
        ContainerA containerA = new ContainerA();
        ContainerB containerB = new ContainerB();
        ContainerC containerC = new ContainerC();

        containerA.setContainerB(containerB);
        containerB.setContainerC(containerC);
        containerC.setContainerA(containerA);
    }

}
