package com.ysu.zyw.constructor;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerC {

    private ContainerA containerA;

    public ContainerC(ContainerA containerA) {
        this.containerA = containerA;
    }

}
