package com.ysu.zyw.constructor;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerA {

    private ContainerB containerB;

    public ContainerA(ContainerB containerB) {
        this.containerB = containerB;
    }

}
