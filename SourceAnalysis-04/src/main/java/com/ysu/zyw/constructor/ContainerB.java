package com.ysu.zyw.constructor;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerB {

    private ContainerC containerC;

    public ContainerB(ContainerC containerC) {
        this.containerC = containerC;
    }

}
