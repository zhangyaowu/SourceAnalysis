package com.ysu.zyw.setter;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerB {

    private ContainerC containerC;

    public ContainerC getContainerC() {
        return containerC;
    }

    public void setContainerC(ContainerC containerC) {
        this.containerC = containerC;
    }

}
