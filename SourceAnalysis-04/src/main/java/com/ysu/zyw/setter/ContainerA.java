package com.ysu.zyw.setter;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerA {

    private ContainerB containerB;

    public ContainerB getContainerB() {
        return containerB;
    }

    public void setContainerB(ContainerB containerB) {
        this.containerB = containerB;
    }

}
