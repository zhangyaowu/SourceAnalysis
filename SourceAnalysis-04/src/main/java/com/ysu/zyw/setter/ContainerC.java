package com.ysu.zyw.setter;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class ContainerC {

    private ContainerA containerA;

    public ContainerA getContainerA() {
        return containerA;
    }

    public void setContainerA(ContainerA containerA) {
        this.containerA = containerA;
    }

}
