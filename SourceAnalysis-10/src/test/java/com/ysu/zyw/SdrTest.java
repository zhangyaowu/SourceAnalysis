package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by ZeRur on 2016/1/10.
 *
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-sdr.xml"})
public class SdrTest {

    @Resource
    private StringRedisSerializer keySerializer;

    @Resource
    private JdkSerializationRedisSerializer valueSerializer;

    @Resource
    private JedisConnectionFactory jedisConnectionFactory;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void test1() {
        User user = new User(1, "123");
        String key = UUID.randomUUID().toString();
        JedisConnection connection = jedisConnectionFactory.getConnection();
        connection.set(keySerializer.serialize(key), valueSerializer.serialize(user));
        System.out.println(valueSerializer.deserialize(connection.get(keySerializer.serialize(key))));
        connection.close();
    }

    @Test
    public void test2() {
        // 使用 redisTemplate 建议指定 serializer 否则会出很多意想不到的序列化情况
        String key = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(key, new User(1, "456"));
        System.out.println(redisTemplate.opsForValue().get(key));
    }

    @Test
    public void test3() throws InterruptedException {
        String key = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(key, new User(1, "135"), 1000, TimeUnit.MILLISECONDS);
        System.out.println(redisTemplate.opsForValue().get(key));
        Thread.sleep(1500);
        System.out.println(redisTemplate.opsForValue().get(key));
    }

    @Test
    public void test4() {
        String key = UUID.randomUUID().toString();
        redisTemplate.opsForList().leftPush(key, new User(1, "123"));
        redisTemplate.opsForList().leftPush(key, new User(2, "234"));
        redisTemplate.opsForList().leftPush(key, new User(3, "345"));
        System.out.println(redisTemplate.opsForList().range(key, 0, -1));
        System.out.println(redisTemplate.opsForList().leftPop(key));
        System.out.println(redisTemplate.opsForList().range(key, 0, -1));
        User user = new User(4, "456");
        redisTemplate.opsForList().leftPush(key, user);
        System.out.println(redisTemplate.opsForList().range(key, 0, -1));
        redisTemplate.opsForList().leftPush(key, user);
        System.out.println(redisTemplate.opsForList().range(key, 0, -1));
    }

    @Test
    public void test5() {
        String key = UUID.randomUUID().toString();
        User user = new User(1, "123");
        redisTemplate.opsForSet().add(key, new User(2, "234"), user, user);
        System.out.println(redisTemplate.opsForSet().members(key));
        // union difference
    }

    @Test
    public void test6() {
        // zset == linkedset
        String key = UUID.randomUUID().toString();
        User user = new User(1, "001");
        // score 类似版本号 新的覆盖旧的更新版本
        redisTemplate.opsForZSet().add(key, user, 1);
        redisTemplate.opsForZSet().add(key, user, 2);
        System.out.println(redisTemplate.opsForZSet().score(key, user));
        System.out.println(redisTemplate.opsForZSet().range(key, 0, -1));
    }

    @Test
    public void test7() {
        String key = UUID.randomUUID().toString();
        Map<String, Object> map = new HashMap<>();
        map.put("key1", new User(1, "11"));
        map.put("key2", new User(2, "22"));
        redisTemplate.opsForHash().putAll(key, map);
        System.out.println(redisTemplate.opsForHash().entries(key));
    }

    @Test
    public void test8() {
        // hyperloglog 计算基数用 会给出大概的不重复元素的个数
        String key = UUID.randomUUID().toString();
        redisTemplate.opsForHyperLogLog().add(key, 1);
        redisTemplate.opsForHyperLogLog().add(key, 2);
        System.out.println(redisTemplate.opsForHyperLogLog().size(key));
        redisTemplate.opsForHyperLogLog().add(key, 1);
        System.out.println(redisTemplate.opsForHyperLogLog().size(key));
    }

    @Test
    public void test9() {
        String key = UUID.randomUUID().toString();
        redisTemplate.opsForValue().getOperations().execute((RedisCallback<Object>) redisConnection -> {
            redisConnection.set(keySerializer.serialize(key), valueSerializer.serialize(144));
            return null;
        });
        Object result = redisTemplate.opsForValue().getOperations().execute(
                (RedisCallback<?>) redisConnection ->
                        valueSerializer.deserialize(redisConnection.get(keySerializer.serialize(key))));
        System.out.println(result);
    }

}
