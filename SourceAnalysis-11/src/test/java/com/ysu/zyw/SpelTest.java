package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.expression.*;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by ZeRur on 2016/1/13.
 *
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-spel.xml"})
public class SpelTest implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 这一段 SPEL 的意思是获取 rootObject 的 length() 然后加上 ‘Hello' 在加上 ’ World‘ 在加上环境中的 #end 变量（!）
     */
    @Test
    public void test1() {
        // rootObject 对谁干
        String rootObject = "ZYW";
        // ExpressionParser 谁来干
        ExpressionParser parser = new SpelExpressionParser();
        // expression 干什么
        Expression expression =
                parser.parseExpression("(length() + 'Hello' + ' World').concat(#end)");
        // EvaluationContext 在哪干
        EvaluationContext context = new StandardEvaluationContext(rootObject);
        context.setVariable("end", "!");
        Assert.isTrue(ObjectUtils.nullSafeEquals(expression.getValue(context), "3Hello World!"));
    }

    /**
     * 方法调用
     */
    @Test
    public void test2() {
        NormalBean normalBean = new NormalBean();
        normalBean.setId(2);
        ExpressionParser parser = new SpelExpressionParser();

        // 普通方法调用
        Expression expression1 = parser.parseExpression("getId()");
        EvaluationContext context1 = new StandardEvaluationContext(normalBean);
        System.out.println(expression1.getValue(context1));

        // 静态方法调用
        Expression expression2 = parser.parseExpression("T(com.ysu.zyw.NormalBean).sayHelo()");
        System.out.println(expression2.getValue());
    }

    /**
     * 变量
     */
    @Test
    public void test3() {
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression1 = parser.parseExpression("#var");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("var", "Hello");
        System.out.println(expression1.getValue(context));

        NormalBean normalBean = new NormalBean();
        Expression expression2 = parser.parseExpression("#normalBean.id = #id");
        context.setVariable("normalBean", normalBean);
        context.setVariable("id", 101);
        System.out.println(expression2.getValue(context));
        System.out.println(normalBean.getId());
    }

    @Test
    public void test4() {
        @SuppressWarnings("serial")
        List<String> list = new ArrayList<String>() {
            {
                add("1");
                add("2");
            }
        };

        @SuppressWarnings("serial")
        Map<Integer, Integer> map = new HashMap<Integer, Integer>() {
            {
                put(1, 11);
                put(2, 22);
            }
        };

        ExpressionParser expressionParser = new SpelExpressionParser();

        EvaluationContext evaluationContext1 = new StandardEvaluationContext(list);

        // list
        String string = expressionParser.parseExpression("[0]").getValue(evaluationContext1, String.class);
        System.out.println("" + string);

        EvaluationContext evaluationContext2 = new StandardEvaluationContext(map);

        // map
        Integer value = expressionParser.parseExpression("[2]").getValue(evaluationContext2, Integer.class);
        System.out.println("" + value);

        // inline list
        @SuppressWarnings("unchecked")
        List<Integer> inlineList = expressionParser.parseExpression("{1, 2, 3, 4}").getValue(List.class);
        System.out.println("" + inlineList);

        // inline map
        @SuppressWarnings("unchecked")
        Map<Integer, String> inlineMap = expressionParser.parseExpression("{1:'1', 2:'2'}").getValue(Map.class);
        System.out.println("" + inlineMap);

        // inline array
        int[] array = expressionParser.parseExpression("new int[] {1, 2, 3}").getValue(int[].class);
        System.out.println("" + Arrays.toString(array));
    }

    /**
     * 安全导航
     */
    @Test
    public void test5() {
        NormalBean normalBean = null;
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("#root?.id");
        EvaluationContext context = new StandardEvaluationContext(normalBean);
        System.out.println(expression.getValue(context));
    }

    /**
     * 投影 (list|map).?[选择表达式]
     * 在一般表达式中 #root 和 #this 是一样的 但在投影表达式中 #root 代表根对象 #this 当前对象
     */
    @Test
    public void test6() {
        List<Integer> rootObject = new ArrayList<>();
        rootObject.add(1);
        rootObject.add(2);
        rootObject.add(3);
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression1 = parser.parseExpression("#root");
        EvaluationContext context = new StandardEvaluationContext(rootObject);

        System.out.println(rootObject);
        System.out.println(expression1.getValue(context));

        // 投影表达式类似 Lambda 的 filter 表达式
        Expression expression2 = parser.parseExpression("#root.?[#this > 1]");
        System.out.println(expression2.getValue(context));
    }

    /**
     * 引用Bean
     */
    @Test
    public void test7() {
        StandardEvaluationContext evaluationContext = new StandardEvaluationContext();
        evaluationContext.setBeanResolver((context, beanName) -> applicationContext.getBean(beanName));
        ExpressionParser expressionParser = new SpelExpressionParser();

        // @beanName 需要context实现beanResolver 使用时会调用 resolver(context, "beanName");
        Object bean = expressionParser.parseExpression("@normalBean").getValue(evaluationContext, Object.class);
        System.out.println(bean);
    }

    @Resource
    private NormalBean normalBean;

    @Resource
    private Container container;

    /**
     * xml 中使用 Spel 规则 #{ <expression string> }
     */
    @Test
    public void test8() {
        System.out.println(normalBean);
        System.out.println(container.getNormalBean());
    }

}
