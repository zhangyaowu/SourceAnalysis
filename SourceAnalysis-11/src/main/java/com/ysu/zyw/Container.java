package com.ysu.zyw;

/**
 * Created by ZeRur on 2016/1/13.
 * @author
 */
public class Container {

    private NormalBean normalBean;

    public NormalBean getNormalBean() {
        return normalBean;
    }

    public void setNormalBean(NormalBean normalBean) {
        this.normalBean = normalBean;
    }

}
