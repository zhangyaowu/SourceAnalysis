package com.ysu.zyw;

import com.ysu.zyw.model.Container;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class Container2StringConverter implements Converter<Container, String> {

    public String convert(Container source) {
        return String.valueOf(source.getId());
    }

}
