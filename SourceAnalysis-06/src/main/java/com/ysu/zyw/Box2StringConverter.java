package com.ysu.zyw;

import com.ysu.zyw.model.Box;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class Box2StringConverter implements Converter<Box, String> {

    public String convert(Box source) {
        return String.valueOf(source.getId());
    }

}
