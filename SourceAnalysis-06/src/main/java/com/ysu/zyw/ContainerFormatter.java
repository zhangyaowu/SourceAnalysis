package com.ysu.zyw;

import com.ysu.zyw.model.Box;
import com.ysu.zyw.model.Container;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class ContainerFormatter implements Formatter<Container> {

    public Container parse(String text, Locale locale) throws ParseException {
        Box box = new Box();
        box.setId(Integer.parseInt(text));
        return box;
    }

    public String print(Container object, Locale locale) {
        return String.valueOf(object.getId());
    }

}
