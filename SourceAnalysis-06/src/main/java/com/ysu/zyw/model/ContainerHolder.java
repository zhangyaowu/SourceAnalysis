package com.ysu.zyw.model;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class ContainerHolder {

    private Container container;

    @Override
    public String toString() {
        return "ContainerHolder{" +
                "container=" + container +
                '}';
    }

    public Container getContainer() {
        return container;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

}
