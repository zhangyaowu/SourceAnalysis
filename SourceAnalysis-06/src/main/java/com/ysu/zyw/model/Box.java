package com.ysu.zyw.model;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class Box extends Container {

    private String name;

    @Override
    public String toString() {
        return "Box{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
