package com.ysu.zyw.model;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class Container {

    private int id;

    @Override
    public String toString() {
        return "Container{" +
                "id=" + id +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
