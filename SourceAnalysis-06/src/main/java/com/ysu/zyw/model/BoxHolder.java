package com.ysu.zyw.model;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class BoxHolder {

    private Box box;

    @Override
    public String toString() {
        return "BoxHolder{" +
                "box=" + box +
                '}';
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

}
