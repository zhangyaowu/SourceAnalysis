package com.ysu.zyw;

import com.ysu.zyw.model.Box;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class String2BoxConverter implements Converter<String, Box> {

    public Box convert(String source) {
        Box box = new Box();
        box.setId(Integer.parseInt(source));
        return box;
    }

}
