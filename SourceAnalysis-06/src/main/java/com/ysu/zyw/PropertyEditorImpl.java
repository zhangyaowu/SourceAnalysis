package com.ysu.zyw;

import com.ysu.zyw.model.Container;

import java.beans.PropertyEditorSupport;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class PropertyEditorImpl extends PropertyEditorSupport {

    private int counter = 56;

    @Override
    public String getAsText() {
        Container container = (Container) this.getValue();
        return String.valueOf(container.getId() - counter);
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Container container = new Container();
        container.setId(Integer.parseInt(text));
        this.setValue(container);
    }

    @Override
    public void setValue(Object value) {
        Container container = (Container) value;
        container.setId(container.getId() + counter);
        super.setValue(container);
    }

    @Override
    public Object getValue() {
        return super.getValue();
    }

}
