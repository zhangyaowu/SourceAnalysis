package com.ysu.zyw;

import com.ysu.zyw.model.Container;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class String2ContainerConverter implements Converter<String, Container> {

    public Container convert(String source) {
        Container container = new Container();
        container.setId(Integer.parseInt(source));
        return container;
    }

}
