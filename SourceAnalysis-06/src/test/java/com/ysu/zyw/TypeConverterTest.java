package com.ysu.zyw;


import com.ysu.zyw.model.Box;
import com.ysu.zyw.model.BoxHolder;
import com.ysu.zyw.model.Container;
import com.ysu.zyw.model.ContainerHolder;
import org.junit.Test;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.format.support.FormattingConversionService;

/**
 * Created by ZeRur on 2016/1/3.
 *
 * @author yaowu.zhang
 */
public class TypeConverterTest {

    private DefaultListableBeanFactory defaultListableBeanFactory;

    {
        defaultListableBeanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        xmlBeanDefinitionReader.loadBeanDefinitions("classpath:application-context-type-converter.xml");
    }

    @Test
    public void testProgramingPropertyEditor() {
        PropertyEditorImpl propertyEditor = new PropertyEditorImpl();
        propertyEditor.setAsText("12");
        System.out.println(propertyEditor.getValue());
        System.out.println(propertyEditor.getAsText());
    }

    @Test
    public void testSpringPropertyEditor() {
        defaultListableBeanFactory.addPropertyEditorRegistrar(new PropertyEditorRegistrar() {
            public void registerCustomEditors(PropertyEditorRegistry registry) {
                registry.registerCustomEditor(Container.class, new PropertyEditorImpl());
            }
        });

        // defaultListableBeanFactory.registerCustomEditor(Container.class, PropertyEditorImpl.class);

        ContainerHolder containerHolder = defaultListableBeanFactory.getBean("containerHolder", ContainerHolder.class);
        System.out.println(containerHolder.getContainer());
    }

    @Test
    public void testSpringPropertyEditorWithChildType() {
        defaultListableBeanFactory.addPropertyEditorRegistrar(new PropertyEditorRegistrar() {
            public void registerCustomEditors(PropertyEditorRegistry registry) {
                registry.registerCustomEditor(Container.class, new PropertyEditorImpl());
            }
        });

        BoxHolder boxHolder = defaultListableBeanFactory.getBean("boxHolder", BoxHolder.class);
        System.out.println(boxHolder.getBox());
    }

    @Test
    public void testProgramingConverter() {
        String2ContainerConverter string2ContainerConverter = new String2ContainerConverter();
        Container2StringConverter container2StringConverter = new Container2StringConverter();

        Container container = string2ContainerConverter.convert("44");
        System.out.println(container);

        String str = container2StringConverter.convert(container);
        System.out.println(str);
    }

    @Test
    public void testProgramingConverterWithChildType() {
        Container2StringConverter container2StringConverter = new Container2StringConverter();

        Box box = new Box();
        box.setId(84);

        String str = container2StringConverter.convert(box);
        System.out.println(str);

        // 对于强类型的 Converter 本身来说 根本不可能 String -> Box
    }

    @Test
    public void testProgramingConversionService() {
        DefaultConversionService defaultConversionService = new DefaultConversionService();
        defaultConversionService.addConverter(new String2ContainerConverter());
        defaultConversionService.addConverter(new Container2StringConverter());

        boolean canConvertString2Container = defaultConversionService.canConvert(String.class, Container.class);
        System.out.println(canConvertString2Container);

        boolean canConvertContainer2String = defaultConversionService.canConvert(Container.class, String.class);
        System.out.println(canConvertContainer2String);

        boolean canConvertString2Box = defaultConversionService.canConvert(String.class, Box.class);
        System.out.println(canConvertString2Box);

        boolean canConvertBox2String = defaultConversionService.canConvert(Box.class, String.class);
        System.out.println(canConvertBox2String);

        // 当使用了 ConversionService 类型的协调会由 ConversionService 来完成
    }


    @Test
    public void testSpringConversionService() {
        DefaultConversionService defaultConversionService = new DefaultConversionService();
        defaultConversionService.addConverter(new String2ContainerConverter());
        defaultConversionService.addConverter(new Container2StringConverter());
        defaultListableBeanFactory.setConversionService(defaultConversionService);

        ContainerHolder containerHolder = defaultListableBeanFactory.getBean("containerHolder", ContainerHolder.class);
        System.out.println(containerHolder.getContainer());
    }

    @Test
    public void testSpringConversionServiceInverse() {
        DefaultConversionService defaultConversionService = new DefaultConversionService();
        defaultConversionService.addConverter(new String2BoxConverter());
        defaultConversionService.addConverter(new Box2StringConverter());
        defaultListableBeanFactory.setConversionService(defaultConversionService);

        BoxHolder boxHolder = defaultListableBeanFactory.getBean("boxHolder", BoxHolder.class);
        System.out.println(boxHolder.getBox());
    }

    @Test
    public void testProgramingFormattingConversionService() {
        FormattingConversionService formattingConversionService = new FormattingConversionService();
        // 注意内部的实现是基于 Box 的
        formattingConversionService.addFormatter(new ContainerFormatter());

        Box box = formattingConversionService.convert("47", Box.class);
        System.out.println(box.getClass() + "-" + box);

        Container container = formattingConversionService.convert("41", Container.class);
        System.out.println(container.getClass() + "-" + container);
    }

    @Test
    public void testSpringFormattingConversionService() {
        FormattingConversionService formattingConversionService = new FormattingConversionService();
        // 注意内部的实现是基于 Box 的
        formattingConversionService.addFormatter(new ContainerFormatter());

        defaultListableBeanFactory.setConversionService(formattingConversionService);

        ContainerHolder containerHolder = defaultListableBeanFactory.getBean("containerHolder", ContainerHolder.class);
        System.out.println(containerHolder.getContainer().getClass() + "-" + containerHolder.getContainer());

        BoxHolder boxHolder = defaultListableBeanFactory.getBean("boxHolder", BoxHolder.class);
        System.out.println(boxHolder.getBox().getClass() + "-" + boxHolder.getBox());
    }

}
