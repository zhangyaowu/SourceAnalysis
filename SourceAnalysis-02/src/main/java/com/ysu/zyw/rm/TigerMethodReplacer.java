package com.ysu.zyw.rm;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
public class TigerMethodReplacer implements MethodReplacer {
    public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
        // do not call method.invoke(obj), because this will cause a loop
        System.out.println("Tiger");
        return null;
    }
}
