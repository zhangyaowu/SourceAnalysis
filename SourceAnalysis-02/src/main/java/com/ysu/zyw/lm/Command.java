package com.ysu.zyw.lm;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
public class Command {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
