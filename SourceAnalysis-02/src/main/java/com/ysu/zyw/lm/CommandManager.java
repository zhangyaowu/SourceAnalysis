package com.ysu.zyw.lm;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
public abstract class CommandManager {

    private Command command;

    public Command createCommand() {
        return command;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

}
