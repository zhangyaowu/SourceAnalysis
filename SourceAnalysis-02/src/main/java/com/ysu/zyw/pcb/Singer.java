package com.ysu.zyw.pcb;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
public class Singer {

    private String name;

    private String instrument;

    @Override
    public String toString() {
        return "Singer{" +
                "name='" + name + '\'' +
                ", instrument='" + instrument + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

}
