package com.ysu.zyw.pcb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-parent-child-bean.xml" })
public class ParentChildBeanTest {

    @Resource
    private Singer singer;

    @Test
    public void testParentChildBean() {
        System.out.println(singer);
    }

}
