package com.ysu.zyw.rm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-replaced-method.xml" })
public class ReplacedMethodTest {

    @Resource
    private Magician magician;

    @Test
    public void testReplacedMethod() {
        magician.getCat();
    }

}
