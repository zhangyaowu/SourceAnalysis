package com.ysu.zyw.lm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-lookup-method.xml" })
public class LookupMethodTest {

    @Resource
    private CommandManager commandManager;

    @Test
    public void testLookupMethod() {
        System.out.println(commandManager.createCommand());
        System.out.println(commandManager.createCommand());
    }

}
