package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZeRur on 2015/12/31.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-startup.xml" })
public class Startup {

    @Resource
    private Container container;

    @Test
    public void test() {
        System.out.println(container);
    }

}
