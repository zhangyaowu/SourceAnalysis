package com.ysu.zyw;

import org.junit.Test;
import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

import java.util.List;
import java.util.Map;

/**
 * Created by ZeRur on 2016/1/1.
 * @author yaowu.zhang
 */
public class StandAlone {

    @Test
    public void testDefaultListableBeanFactory() {
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        BeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        beanDefinitionReader.loadBeanDefinitions("classpath:application-context-startup.xml");
        Container container = defaultListableBeanFactory.getBean("container", Container.class);
        System.out.println(container);
        System.out.println(container.getBox());
        defaultListableBeanFactory.destroySingletons();
    }

    @Test
    public void testPrototype() {
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        BeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        beanDefinitionReader.loadBeanDefinitions("classpath:application-context-startup.xml");
        @SuppressWarnings(value = "unchecked")
        List<Container> container = defaultListableBeanFactory.getBean("container", List.class);
        System.out.println(container);
        defaultListableBeanFactory.destroySingletons();
    }

}
