package com.ysu.zyw;

/**
 * Created by ZeRur on 2015/12/31.
 * @author yaowu.zhang
 */
public class Container {

    private Box box;

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }
}
