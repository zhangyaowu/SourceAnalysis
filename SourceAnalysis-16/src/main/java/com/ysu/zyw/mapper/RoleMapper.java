package com.ysu.zyw.mapper;

import com.ysu.zyw.domain.Role;

/**
 * Created by ZeRur on 2016/1/16.
 *
 * @author yaowu.zhang
 */
public interface RoleMapper {

    public Role selectByPrimaryKey(String id);

}
