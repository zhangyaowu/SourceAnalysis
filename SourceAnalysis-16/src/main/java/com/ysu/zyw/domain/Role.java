package com.ysu.zyw.domain;

/**
 * Created by ZeRur on 2016/1/16.
 *
 * @author yaowu.zhang
 */
public class Role {

    private String id;

    private String role;

    @Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
