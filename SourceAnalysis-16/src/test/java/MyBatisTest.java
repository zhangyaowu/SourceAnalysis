import com.ysu.zyw.domain.Role;
import com.ysu.zyw.mapper.RoleMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.jdbc.Sql;

import java.io.IOException;
import java.sql.Connection;

/**
 * Created by ZeRur on 2016/1/16.
 * @author yaowu.zhang
 */
public class MyBatisTest {

    @Test
    public void test1() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(new ClassPathResource("mybatis-config.xml").getInputStream());
        RoleMapper roleMapper = sqlSessionFactory.openSession().getMapper(RoleMapper.class);
        Role role = roleMapper.selectByPrimaryKey("R_04a3b6d09e2449c994450387a68919f3");
        System.out.println(role);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        sqlSession.getConnection();
    }

}
