package com.ysu.zyw;

import org.junit.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class BeanPostProcessorsTest {

    @Test
    public void testBeanPostProcessors() {
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        xmlBeanDefinitionReader.loadBeanDefinitions("classpath:application-context-bean-post-processors.xml");

        defaultListableBeanFactory.addBeanPostProcessor(new BeanPostProcessImpl());
        defaultListableBeanFactory.addBeanPostProcessor(new MergedBeanDefinitionPostProcessorImpl());
        defaultListableBeanFactory.addBeanPostProcessor(new InstantiationAwareBeanPostProcessorImpl());
        defaultListableBeanFactory.addBeanPostProcessor(new SmartInstanitationAwareBeanPostProcessorImpl());

        Box box = defaultListableBeanFactory.getBean("box", Box.class);
        System.out.println(box);
    }

}
