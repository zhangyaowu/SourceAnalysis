package com.ysu.zyw;

import java.util.List;

/**
 * Created by ZeRur on 2016/1/2.
 * @author yaowu.zhang
 */
public class Box {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
