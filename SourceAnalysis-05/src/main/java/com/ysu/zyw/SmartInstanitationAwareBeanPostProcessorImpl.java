package com.ysu.zyw;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class SmartInstanitationAwareBeanPostProcessorImpl implements SmartInstantiationAwareBeanPostProcessor {

    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInstantiation(" + beanClass + ", " + beanName + ")");
        return null;
    }

    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return true;
    }

    public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + pvs + ", " + bean + ", " + beanName + ")");
        return pvs;
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

    public Class<?> predictBeanType(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".predictBeanType(" + beanClass + ", " + beanName + ")");
        return null;
    }

    public Constructor<?>[] determineCandidateConstructors(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".determineCandidateConstructors(" + beanClass + ", " + beanName + ")");
        return null;
    }

    public Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".getEarlyBeanReference(" + bean + ", " + beanName + ")");
        return bean;
    }

}
