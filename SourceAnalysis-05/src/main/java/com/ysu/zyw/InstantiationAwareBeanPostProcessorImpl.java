package com.ysu.zyw;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

import java.beans.PropertyDescriptor;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class InstantiationAwareBeanPostProcessorImpl implements InstantiationAwareBeanPostProcessor {

    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInstantiation(" + beanClass + ", " + beanName + ")");
        return null;
    }

    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return true;
    }

    public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + pvs + ", " + bean + ", " + beanName + ")");
        return pvs;
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

}
