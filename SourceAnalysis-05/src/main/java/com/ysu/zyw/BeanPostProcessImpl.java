package com.ysu.zyw;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class BeanPostProcessImpl implements BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

}
