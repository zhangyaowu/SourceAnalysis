package com.ysu.zyw;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * Created by ZeRur on 2016/1/3.
 * @author yaowu.zhang
 */
public class MergedBeanDefinitionPostProcessorImpl implements MergedBeanDefinitionPostProcessor {

    public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
        System.out.println(this.getClass().getName() + ".postProcessMergedBeanDefinition(" + beanDefinition + ", " + beanType + ", " + beanName + ")");
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessBeforeInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(this.getClass().getName() + ".postProcessAfterInitialization(" + bean + ", " + beanName + ")");
        return bean;
    }

}
