import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.junit.Test;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ZeRur on 2016/1/10.
 * @author yaowu.zhang
 */
public class TransactionTest {

    @Test
    public void test1() throws Exception {
        // 初始化 DataSource
        Map<String, Object> dataSourceProperties = new HashMap<String, Object>();
        dataSourceProperties.put("username", "root");
        dataSourceProperties.put("password", "root");
        dataSourceProperties.put("url", "jdbc:mysql://127.0.0.1:3306/localhost_user");

        DataSource dataSource = DruidDataSourceFactory.createDataSource(dataSourceProperties);

        // 初始化 TransactionManager
        PlatformTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);

        // 开始 Transaction
        TransactionStatus transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

        // 获取 Connection 只要是在同一个线程 拿到的都是同一个 Connection
        @SuppressWarnings("unused")
        Connection connection = DataSourceUtils.getConnection(dataSource);

        // biz

        // 提交事务
        transactionManager.commit(transactionStatus);
    }

    @Test
    public void test2() throws Exception {
        // 初始化 DataSource
        Map<String, Object> dataSourceProperties = new HashMap<String, Object>();
        dataSourceProperties.put("username", "root");
        dataSourceProperties.put("password", "root");
        dataSourceProperties.put("url", "jdbc:mysql://127.0.0.1:3306/localhost_user");

        DataSource dataSource = DruidDataSourceFactory.createDataSource(dataSourceProperties);

        // 初始化 TransactionManager
        PlatformTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);

        // 初始化 TransactionTemplate
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);

        transactionTemplate.execute(TransactionStatus -> {
            // 获取 Connection 这种方式依赖于 TransactionManager 的实现 不推荐
            ConnectionHolder connectionHolder = (ConnectionHolder) TransactionSynchronizationManager
                    .getResource(dataSource);
            @SuppressWarnings("unused")
            Connection connection = connectionHolder.getConnection();

            // biz

            return null;
        });
    }
}
