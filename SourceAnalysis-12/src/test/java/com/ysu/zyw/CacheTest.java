package com.ysu.zyw;

import com.ysu.zyw.cs.Article;
import com.ysu.zyw.cs.ArticleDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * Created by ZeRur on 2016/1/11.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-cache.xml"})
public class CacheTest {

    /**
     * 自己实现的 MapCacheImpl
     */
    @Test
    public void test1() {
        Cache cache = new MapCacheImpl();
        String key = UUID.randomUUID().toString();
        System.out.println(cache.get(key, String.class));
        cache.put(key, key);
        System.out.println(cache.get(key, String.class));
    }

    /**
     * Spring 提供的基于 ConcurrentHashMap Cache 实现
     */
    @Test
    public void test2() {
        Cache cache = new ConcurrentMapCache("cache_name");
        String key = UUID.randomUUID().toString();
        System.out.println(cache.get(key, String.class));
        cache.put(key, key);
        System.out.println(cache.get(key, String.class));
    }


    @Resource
    private StringRedisSerializer keySerializer;

    @SuppressWarnings("unused")
    @Resource
    private JdkSerializationRedisSerializer valueSerializer;

    @SuppressWarnings("unused")
    @Resource
    private JedisConnectionFactory jedisConnectionFactory;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * Spring Data Redis 提供的基于 Redis Cache 实现
     */
    @Test
    public void test3() {
        Cache cache = new RedisCache("redis_cache_name", keySerializer.serialize("KEY_"), redisTemplate, 10);
        String key = UUID.randomUUID().toString();
        cache.put(key, key);
        System.out.println(cache.get(key, String.class));
    }

    @Resource
    private CacheManager cacheManager;

    @Test
    public void test4() {
        System.out.println(cacheManager.getCache("default"));
        System.out.println(cacheManager.getCache("backup"));

    }

    /**
     * Cache / Spel
     */

    @Resource
    private ArticleDao articleDao;

    @Test
    public void test5() {
        // 使用 default / backup Cache 做缓存 key 为参数 articleId 条件参数 articleId < 15 除去结果 id = 10

        // 未缓存 走 db
        System.out.println(articleDao.select("1"));
        // 已缓存 走缓存
        System.out.println(articleDao.select("1"));

        System.out.println();

        // 未缓存 走 db 不符合 condition 条件 未缓存
        System.out.println(articleDao.select("16"));
        // 未缓存 走 db
        System.out.println(articleDao.select("16"));

        System.out.println();

        // 未缓存 走 db 不符合 unless 条件 未缓存
        System.out.println(articleDao.select("10"));
        // 未缓存 走 db
        System.out.println(articleDao.select("10"));

        System.out.println();

        // 未缓存 走 db
        System.out.println(articleDao.select("2"));
        // 已缓存 走缓存
        System.out.println(articleDao.select("2"));
        // 清空一级缓存
        cacheManager.getCache("default").clear();
        // 穿透一级缓存走二级缓存
        System.out.println(articleDao.select("2"));
        // 清空二级缓存
        cacheManager.getCache("backup").clear();
        // 缓存穿透 走 db
        System.out.println(articleDao.select("2"));
    }

    @Test
    public void test6() {
        // 使用 default / backup Cache 做缓存, key 为参数 articleId, 条件为参数 article.id < 15
        // 不符合条件 不缓存
        articleDao.insert(new Article("35", "article - 35"));
        // 未缓存 走 db
        System.out.println(articleDao.select("35"));

        System.out.println();

        // 缓存
        articleDao.insert(new Article("5", "article - 5"));
        // 缓存 走缓存
        System.out.println(articleDao.select("5"));
    }

    @Test
    public void test7() {
        // 清除缓存
        articleDao.delete("12");
        // 清楚了缓存 / 删除了数据 应该返回 null
        System.out.println(articleDao.select("12"));
    }

    @Test
    public void test8() {
        // 更新缓存
        articleDao.update(new Article("4", "article - 4 - modify"));
        // 从缓存中获取
        System.out.println(articleDao.select("4"));
    }

}
