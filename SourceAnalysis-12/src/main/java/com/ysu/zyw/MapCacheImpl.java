package com.ysu.zyw;

import org.springframework.cache.Cache;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ZeRur on 2016/1/12.
 * @author yaowu.zhang
 */
public class MapCacheImpl implements Cache {

    private static final String CACHE_NAME = "map_cache_impl";

    private static final Map<java.lang.Object, Object> pool = new ConcurrentHashMap<>();

    @Override
    public String getName() {
        return CACHE_NAME;
    }

    @Override
    public Object getNativeCache() {
        Assert.isTrue(false, "do not use this");
        return null;
    }

    @Override
    public ValueWrapper get(Object key) {
        Assert.isTrue(false, "do not use this");
        return null;
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        Object value = pool.get(key);
        if(value == null)
            return null;
        Assert.isAssignable(type, value.getClass());
        return (T) value;
    }

    @Override
    public void put(Object key, Object value) {
        pool.put(key, value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        Assert.isTrue(false, "do not use this");
        return null;
    }

    @Override
    public void evict(Object key) {
        pool.remove(key);
    }

    @Override
    public void clear() {
        pool.clear();
    }
}
