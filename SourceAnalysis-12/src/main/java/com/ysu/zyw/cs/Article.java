package com.ysu.zyw.cs;

import java.io.Serializable;

/**
 * Created by ZeRur on 2016/1/13.
 *
 * @author yaowu.zhang
 */
public class Article implements Serializable {

    private String id;

    private String name;

    public Article() {
    }

    public Article(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
