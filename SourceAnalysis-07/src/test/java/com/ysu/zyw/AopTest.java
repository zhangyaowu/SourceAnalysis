package com.ysu.zyw;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.TargetSource;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.target.HotSwappableTargetSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sun.management.HotspotClassLoadingMBean;

import javax.annotation.Resource;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by ZeRur on 2016/1/4.
 * @author yaowu.zhang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-aop.xml" })
public class AopTest {

    /**
     * ProxyFactory 系列
     */

    @Test
    public void testProxyFactory1() {
        ProxyFactory proxyFactory = new ProxyFactory();
        // 这里使用的是 DefaultPointcut, 它内部持有一个 TruePointcut 作为 pointcut, 而 Advice 是我们外部传入的 Lambda, 类型是一个 MethodBeforeAdvice
        proxyFactory.addAdvisor(new DefaultPointcutAdvisor((MethodBeforeAdvice) (method, args, target) ->
                System.out.println("before ...")
        ));
        // 这里是一个代理类, Spring 内部默认使用 SingletonTargetSource 包装, 每次返回的都是代理类自身
        proxyFactory.setTarget(new Man());
        // 这里我们使用 JDK Proxy 模式进行代理
        proxyFactory.setProxyTargetClass(false);
        // 对于使用 JDK Proxy 模式进行代理需要一个必要条件, 要代理的接口
        proxyFactory.setInterfaces(Speakable.class);

        // 获取 Proxy
        Speakable speaker = (Speakable) proxyFactory.getProxy();
        // 调用代理类的逻辑
        speaker.speak();

        // 这里是重点, 我们使用的是 DefaultPointcutAdvisor, 它内部包含的 TruePointcut, toString() 方法也是会被代理的方法之一
        speaker.toString();

        // 可以看到类型是 com.sun.proxy.$... 这是 JDK Proxy 的代表性类型签名
        System.out.println(speaker.getClass());
    }

    @Test
    public void testProxyFactory2() {
        ProxyFactory proxyFactory = new ProxyFactory();
        // 同上
        proxyFactory.addAdvisor(new DefaultPointcutAdvisor((MethodBeforeAdvice) (method, args, target) ->
                System.out.println("before ...")
        ));
        // 使用 CGLIB 模式进行代理
        proxyFactory.setProxyTargetClass(true);
        proxyFactory.setTarget(new Man());

        Speakable speaker = (Speakable) proxyFactory.getProxy();
        speaker.speak();

        speaker.toString();

        System.out.println(speaker.getClass());
    }

    /**
     * AspectJProxyFactory 系列
     */

    @Test
    public void testAspectJProxyFactory1() {
        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory();
        aspectJProxyFactory.setTarget(new Man());
        aspectJProxyFactory.setProxyTargetClass(false);
        aspectJProxyFactory.setInterfaces(Speakable.class);

        // 使用 addAdvice 则 Factory 会包装成为一个 DefaultPointcutAdvisor
        aspectJProxyFactory.addAdvice((MethodBeforeAdvice) (method, args, target) ->
                System.out.println("before ...")
        );
        Speakable speaker = aspectJProxyFactory.getProxy();

        speaker.speak();

        System.out.println(speaker);
    }

    @Test
    public void testAspectJProxyFactory2() {
        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory();
        aspectJProxyFactory.setTarget(new Man());
        aspectJProxyFactory.setProxyTargetClass(true);

        // 指定 Advisor
        AspectJExpressionPointcutAdvisor aspectJExpressionPointcutAdvisor = new AspectJExpressionPointcutAdvisor();
        aspectJExpressionPointcutAdvisor.setAdvice((MethodBeforeAdvice) (method, args, target) ->
                System.out.println("before ...")
        );
        aspectJExpressionPointcutAdvisor
                .setExpression("execution(public * com.ysu.zyw.Man.speak(..))");

        aspectJProxyFactory.addAdvisor(aspectJExpressionPointcutAdvisor);

        Speakable speaker = aspectJProxyFactory.getProxy();

        speaker.speak();

        // 使用 AspectJExpressionPointcutAdvisor 之后 toString 方法不会被代理
        speaker.toString();
    }

    /**
     * ProxyFactoryBean 系列
     */

    @Resource(name = "jdkProxyMan")
    private Speakable speaker1;

    @Test
    public void testProxyFactoryBean1() {
        speaker1.speak();

        System.out.println(speaker1.getClass());
    }

    @Resource(name = "cglibProxyMan")
    private Speakable speaker2;

    @Test
    public void testProxyFactoryBean2() {
        speaker2.speak();

        System.out.println(speaker2.getClass());
    }


    /**
     * TargetSource
     */

    public static class Source {

        private int id;

        private int counter = 2;

        public Source(int id) {
            this.id = id;
        }

        public int call() {
            System.out.println("counter-" + id + ", left use count-" + --counter);
            return counter;
        }
    }

    @Test
    public void testTargetSource() {
        // 一个资源只能使两次, 两次之后就换资源, 不用重新生成代理
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvisor(new DefaultPointcutAdvisor((MethodBeforeAdvice) (method, args, target) ->
                System.out.println("before ...")
        ));
        proxyFactory.setProxyTargetClass(true);
        HotSwappableTargetSource targetSource = new HotSwappableTargetSource(new Source(0));
        proxyFactory.setTargetSource(targetSource);

        Source source = (Source) proxyFactory.getProxy();

        for(int i = 0; i < 10; i++) {
            int counter = source.call();
            if(counter == 0) {
                targetSource.swap(new Source(i));
                System.out.println("swap source ...");
            }
        }
    }

}
