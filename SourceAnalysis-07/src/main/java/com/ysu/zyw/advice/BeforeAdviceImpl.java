package com.ysu.zyw.advice;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * Created by ZeRur on 2016/1/6.
 * @author yaowu.zhang
 */
public class BeforeAdviceImpl implements MethodBeforeAdvice {

    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println(this.getClass().getName() + "-before");
    }

}
