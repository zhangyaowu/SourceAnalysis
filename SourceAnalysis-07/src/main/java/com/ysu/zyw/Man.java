package com.ysu.zyw;

/**
 * Created by ZeRur on 2016/1/6.
 * @author yaowu.zhang
 */
public class Man implements Speakable {

    @Override
    public void speak() {
        System.out.println("man speak ...");
    }

}
