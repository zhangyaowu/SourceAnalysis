package com.ysu.zyw;

/**
 * Created by ZeRur on 2016/1/6.
 * @author yaowu.zhang
 */
public interface Speakable {

    public void speak();

}
